<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // user com editor
        $editor = User::create([    
            'name' => 'editor',
            'email' => 'editor@email.com',
            'password' => bcrypt('123456')
        ]);

        $editor->assignRole('editor');

        // user com moderador
        $moderador = User::create([    
            'name' => 'moderador',
            'email' => 'moderador@email.com',
            'password' => bcrypt('123456')
        ]);

        $moderador->assignRole('moderador');

        // user com super-admin
        $admin = User::create([    
            'name' => 'admin',
            'email' => 'admin@email.com',
            'password' => bcrypt('123456')
        ]);

        $admin->assignRole('super-admin');
    }
}
