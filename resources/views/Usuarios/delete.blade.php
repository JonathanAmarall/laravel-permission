<form action="{{ route('usuarios.destroy', $user) }}" method="post" style="display:inline-block">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-danger">Excluir</button>
</form>