@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header">
                    <h1>Lista de usuários</h1>
                </div>
                <div class="card-body">
                    <div class="">
                        <!-- Por funcao: @can('update user')  -->
                        @role('super-admin') 
                          <a href="{{ route('usuarios.create') }}" class="btn btn-success float-right">Add user</a>
                        @else  
                          <a href="#" class="btn btn-success float-right disabled" aria-disabled="true">Add user</a>
                        @endrole   
                        <!-- Por funcao: @endcan('update user')  -->          
                    </div>
                    <table class="table">
                        <thead>
                            <th>Name: </th>
                            <th>Email: </th>
                            <th>Funções: </th>
                            <th>Actions: </th>

                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->roles->implode('name', ',') }}</td>
                                <td>
                                    @role('super-admin')
                                        <a href="{{ route('usuarios.edit', $user) }}" class="btn btn-dark">Editar</a>
                                        <!-- Inclui template contendo form com metodo delete -->
                                        @include('usuarios.delete', ['user' => $user])
                                    @else
                                        Sem ação
                                    @endrole
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>


    </div>
</div>



@endsection