@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Novo usuário:</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('usuarios.store') }}" method="post">
                        @csrf
                        <div class="form-group">
                            Name:
                            <input type="text" name="name" class="form-control" required>
                        </div>

                        <div class="form-group">
                            Email:
                            <input type="email" name="email" class="form-control" required>
                        </div>

                        <div class="form-group">
                            Password:
                            <input type="password" name="password" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="roles">Funções</label>
                            <select class="form-control" name="role">
                                @foreach($roles as $key => $value)
                                <option value="{{ $value }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary " value="Enviar">
                            <a href="{{ route('usuarios.index') }}" class="btn btn-danger">Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection