<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Protegendo rota para apenas super-admin poder acessa-la. Mais em: https://docs.spatie.be/laravel-permission/v2/basic-usage/middleware/
// Route::group(['middleware' => ['role:super-admin']], function () {
    Route::resource('/usuarios', 'UserController');
// });
