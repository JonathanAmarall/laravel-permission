<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        // metodo implode une itens de uma colection e separa pelo caracter passado no segundo parametro
        $rol = $user->roles->implode('name', ',');

        switch ($rol) {
            case 'super-admin':
                $saudacao = "Bem-vindo Super-admin";
                return view('home', compact('saudacao'));
                break;

            case 'moderador':
            case 'super-admin':
                $saudacao = "Bem-vindo Moderador";
                return view('home', compact('saudacao'));
                break;

            case 'editor':
            case 'super-admin':
                $saudacao = "Bem-vindo Editor";
                return view('home', compact('saudacao'));
                break;
        }
    }
}
