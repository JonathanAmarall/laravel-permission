<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        // protegendo controllers e metodos
        //quem possui permissao create user só pode criar e 
        $this->middleware(['permission:create user'], ['only' => ['create', 'store']]);
        //quem só pode read users só vai ter acesso ao index
        $this->middleware(['permission:read users'], ['only' => 'index']);
        //quem pode editar usuario pode criar 
        $this->middleware(['permission:update user'], ['only' => ['edit', 'update']]);
        //só podera deletar
        $this->middleware(['permission:delete user'], ['only' => 'delete']);
    }




    public function index()
    {
        $users = User::all();
        return view('usuarios.index', compact('users'));
    }


    public function create()
    {
        $roles = Role::all()->pluck('name', 'id');
        return view('usuarios.create', compact('roles'));
    }


    public function store(Request $request)
    {
        $usuario = new User;

        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);

        if ($usuario->save()) {
            # code...
            $usuario->assignRole($request->role);

            return redirect()->route('usuarios.index');
        }
    }




    public function edit($id)
    {
        $roles = Role::all()->pluck('name', 'id');
        $usuario = User::findOrFail($id);

        return view('usuarios.edit', compact('usuario', 'roles'));
    }


    public function update(Request $request, $id)
    {
        $usuario = User::findOrFail($id);
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        //verifica se o input do password vem preenchido e atualiza
        if ($request->password != null) {
            $usuario->password = $request->password;
        }
        //substitui role antigo pelo novo
        $usuario->syncRoles($request->role);

        $usuario->save();

        return redirect()->route('usuarios.index');
    }


    public function destroy($id)
    {
        $usuario = User::findOrFail($id);

        // Excluir Role
        $usuario->removeRole($usuario->roles->implode('name', ','));
        // Excluir usuario
        if ($usuario->delete()) {
            return redirect()->route('usuarios.index');
        } else {
            return response()->json([
                'Mensagem' => 'Erro ao excluir usuário'
            ]);
        }
    }
}
